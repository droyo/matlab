function R = grep(S, reg)
% Return the subarray of strings in cellstr S that match reg
	idx = regexp(S, reg, 'once');
	R = S(~cellfun(@isempty, idx));
end
