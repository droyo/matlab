function R = findreg(path, reg, depth)
% FINDREG Search path for matching file names
%  
%  Findreg(path,reg,depth) searches the directory (or cell
%  array of directories) 'path' matching the regular 
%  expression 'reg'.
%  
%  If depth is provided, findreg will search subdirectories
%  to the desired depth. If depth 0, or depth is not
%  specified, findreg will perform a full search of each
%  subdirectory.
%  
%  Find returns a cellstr of the absolute
%  path for each matching file. If no files are found,
%  an empty cell array is returned.

	if nargin < 3, depth = 0; end
	if nargin < 2, reg = '.'; end
	
	assert(depth >= 0, 'Depth must be zero or more.');
	assert(ischar(reg));
	assert(ischar(path) || iscellstr(path));
	
	path = cellstr(path);
	
	function R = main(path, depth)
		y = dir(path);
		if length(y) > 0
			files = str.grep({y(~[y.isdir]).name}, reg);
			files = cellfun(@(f)fullfile(path,f),files,'UniformOutput',false);
			dirs  = str.grep({y([y.isdir]).name},'^[^(\.\.?)]');
	
			if abs(depth-1) > 0 && length(dirs) > 0
				walk = @(d) main(fullfile(path, d), depth-1);
				child = cellfun(walk, dirs, 'UniformOutput', false);
				R = horzcat(files{:}, child{:});
			else
				R = files;
			end
		end
	end
	R = cellfun(@(x)main(x, depth), path,'UniformOutput',false);
	R = R{:}';
end
