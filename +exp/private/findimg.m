function R = findimg(path, depth)
	if nargin < 2, depth = 0; end
	R = dir.findreg(path, '.*\.(jpeg|jpg|png|gif|tiff|JPEG|JPG|PNG|GIF|TIFF)$', depth);
end
