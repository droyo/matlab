This is a series of matlab namespaces. Add this root directory to your
matlab path, and execute functions under +pkg like so:

	pkg.func1(args ...)

INDEX

+exp	psychology experiments using psychtoolbox
+ptb	psychtoolbox helper functions
+net	networking functions
+mat	useful matrix operations
+dir	operations dealing with files and directories
+pic	routines to manipulate pictures
+str	routines for strings and cellstr